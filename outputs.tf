output "name" {
  value = "${google_dns_managed_zone.zone.name}"
}

output "dns_name" {
  value = "${google_dns_managed_zone.zone.dns_name}"
}

output "name_servers" {
  value = "${google_dns_managed_zone.zone.name_servers}"
}
