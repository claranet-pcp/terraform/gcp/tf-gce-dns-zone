resource "google_dns_managed_zone" "zone" {
  name        = "${var.zone_name}"
  dns_name    = "${var.domain}"
  description = "DNS zone created and managed by Terraform."
}
